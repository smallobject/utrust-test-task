export type EthereumWallet = {
  address: string,
  balance: string,
}