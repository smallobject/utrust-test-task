// import vendors
import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

// import shared
import Success from '../../shared/Success.svg';

// import API
import { fakeAPICall } from '../../api/fakeApi';

type HistoryState = {
  amount: number;
  clientAddress: string;
  destinationAddress: string;
};

const SuccessForm = () => {
  const { state } = useLocation<HistoryState>();
  const { clientAddress, destinationAddress, amount } = state

  useEffect(() => {
    fakeAPICall(clientAddress, destinationAddress, amount);
  }, [])

  return (
    <div className='w-[404px] h-[444px] bg-brand-shade'>
      <div className='text-[20px] text-brand-black bg-white rounded-tr-md rounded-tl-md py-5 px-5 font-medium border-b border-brand-gray'>
        Transaction complete
      </div>
      <div className='flex justify-center mt-6'>
        <img src={Success} alt='success_svg' />
      </div>
      <div className='ml-6 mt-8 text-brand-black'>
        <div className='text-base'>You Sent</div>
        <div className='text-[28px]'>{amount} ETH</div>
        <div className='border-b border-brand-black w-2/6 mt-5' />
        <div className='text-base mt-4'>
          From
          <div className='text-xs'>{clientAddress}</div>
        </div>
        <div className='text-base mt-4'>
          To
          <div className='text-xs'>{destinationAddress}</div>
        </div>
      </div>
    </div>
  );
};

export default SuccessForm;
