// import from vendors
import React, { useState } from 'react';
import { useLocation, useHistory } from 'react-router';
import clsx from 'clsx';

import { EthereumWallet } from '../../types/Wallets';

// import components
import Button from '../ui/Button/Button';

const SendCryptoFrom = () => {
  const { state } = useLocation<EthereumWallet>();
  const history = useHistory();

  const [clientAddress, setClientAddress] = useState(state.address);
  const [destinationAddress, setDestinationAddress] = useState('');
  const [amount, setAmount] = useState('');

  const handleGoBack = () => {
    history.goBack();
  };

  const handleSend = () => {
    history.push('/send/success', {clientAddress, destinationAddress, amount})
  }

  return (
    <div className='w-[404px] h-[444px] bg-brand-shade'>
      <div className='text-[20px] text-brand-black bg-white rounded-tr-md rounded-tl-md py-5 px-5 font-medium border-b border-brand-gray'>
        Please fill the form to send Ethereum
      </div>
      <div className=' flex flex-col items-center'>
        {/* We would have a form here usually but we're not really doing any API requests */}
        <label className='text-xs text-brand-black mb-2 mt-[28px] ml-10 self-start'>
          From
        </label>
        <input
          className='rounded-md border border-brand-main w-[356px] h-[44px] ml-3 placeholder-brand-black placeholder-opacity-60 hover:cursor-not-allowed'
          placeholder='Your address'
          type='text'
          value={clientAddress}
          disabled
        />
        <label className='text-xs text-brand-black mb-2 mt-[20px] ml-10 self-start'>
          To
        </label>
        <input
          className='rounded-md border border-brand-main w-[356px] h-[44px] ml-3 placeholder-brand-black placeholder-opacity-60'
          placeholder='Destination address'
          type='text'
          value={destinationAddress}
          onChange={(e) => setDestinationAddress(e.target.value)}
        />
        <label className='text-xs text-brand-black mb-2 mt-[20px] ml-10 self-start'>
          Amount
        </label>
        <input
          className={clsx(
            'rounded-md border border-brand-main w-[356px] h-[44px] ml-3 placeholder-opacity-60 placeholder-brand-black',
            amount && 'font-semibold'
          )}
          onChange={(e) => setAmount(e.target.value)}
          placeholder='Ethereum amount'
          type='number'
          value={amount}
          step={0.01}
        />
        <div className='self-start text-xs text-brand-black ml-8 mt-1 font-medium flex'>
          Balance: {state.balance} ETH
          <div
            className='ml-2 text-brand-maintext-xs text-brand-main hover:cursor-pointer hover:underline transition-all'
            onClick={() => setAmount(state.balance)}
          >
            Use All?
          </div>
        </div>
      </div>
      <div className='flex justify-between m-6'>
        <Button title='Back' onClickHandler={handleGoBack} />
        <Button
          title='Send'
          disabled={!amount || !destinationAddress}
          onClickHandler={handleSend}
        />
      </div>
    </div>
  );
};

export default SendCryptoFrom;
