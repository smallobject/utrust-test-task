// import vendors
import React, { FC } from 'react';
import clsx from 'clsx';

type Props = {
  title: string;
  disabled?: boolean;
  extraClasses?: string;
  onClickHandler?: () => void;
};

const Button: FC<Props> = ({
  title,
  disabled,
  extraClasses,
  onClickHandler,
}) => {
  return (
    <div
      className={clsx(
        'flex items-center justify-center w-[89px] h-10 rounded-[100px] bg-brand-main hover:cursor-pointer hover:ring-4 hover:ring-brand-gray',
        disabled && 'bg-opacity-20 hover:cursor-not-allowed hover:ring-0',
        extraClasses
      )}
      onClick={onClickHandler}
    >
      <div className='text-xs text-white'>{title}</div>
    </div>
  );
};

export default Button;
