// import vendors
import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { useHistory } from 'react-router';

// import components
import Button from '../ui/Button/Button';
import data from '../../data.json';

// import types
import { EthereumWallet } from '../../types/Wallets';

const WalletAddresses = () => {
  const [selected, setSelected] = useState<EthereumWallet>();
  const history = useHistory();
  
  const handleSelected = (item: EthereumWallet) => {
    setSelected(item);
  };

  const handleNextButton = () => {
    history.push('/send', { ...selected });
  };

  return (
    <div className='w-[480px] h-[440px]'>
      <div className='text-[20px] text-brand-black bg-white rounded-tr-md rounded-tl-md my-5 mx-5 font-medium'>
        My Ethereum addresses
      </div>
      <div className='flex flex-col border-t border-b border-brand-gray'>
        {data.map((item, idx) => (
          <div
            key={idx}
            className={clsx(
              'border-b border-brand-gray flex justify-between py-6 px-4 items-center hover:cursor-pointer transition-colors',
              'hover:bg-brand-main hover:bg-opacity-30',
              selected?.address.includes(item.address)
                ? 'bg-brand-main bg-opacity-30'
                : 'bg-brand-shade'
            )}
            onClick={() => handleSelected(item)}
          >
            <div className='text-brand-black text-[12px] text-opacity-80'>{item.address}</div>
            <div className='font-bold text-brand-black flex text-lg'>
              {item.balance}
              <div className='font-normal text-xs ml-1.5 self-center'>ETH</div>
            </div>
          </div>
        ))}
      </div>
      <div className='flex items-center m-5 justify-between'>
        <div className='text-[12px] text-brand-black'>
          Please select the address you wish to send money from.
        </div>
        <Button
          title='Next'
          disabled={!selected}
          onClickHandler={handleNextButton}
        />
      </div>
    </div>
  );
};

export default WalletAddresses;
