// import vendors
import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// import layout
import MainContainer from './layouts/MainContainer';

// import component
import WalletAddresses from './components/WalletAddresses/WalletAddresses';
import SendCryptoForm from './components/SendCryptoForm/SendCryptoForm';
import SuccessForm from './components/SuccessForm/SuccessForm';

const App = () => {
  return (
    <Router>
      <MainContainer>
        <Route exact path='/'>
          <WalletAddresses />
        </Route>
        <Route exact path='/send'>
          <SendCryptoForm />
        </Route>
        <Route path='/send/success'>
          <SuccessForm />
        </Route>
      </MainContainer>
    </Router>
  );
};

export default App;
