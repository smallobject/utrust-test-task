// import vendors
import React, { FC } from 'react';
import Logo from '../shared/Logo.svg';
const MainContainer: FC = ({ children }) => {
  return (
    <div className='bg-white flex w-screen h-screen items-center justify-center -mt-16'>
      <div className='flex flex-col'>
        <img src={Logo} className='w-28 h-7 mb-6 ml-4' />
        <div className='border border-brand-borders rounded-md shadow-lg'>
        {children}
        </div>
      </div>
    </div>
  );
};

export default MainContainer;
