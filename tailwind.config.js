const colors = require('tailwindcss/colors');

module.exports = {
  mode: 'jit',
  purge: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        gray: colors.gray,
        brand: {
          main: '#6932D4',
          shade: '#F5F5FF',
          gray: '#E0E0F4',
          black: '#202532',
          borders: 'rgba(105, 50, 212, 0.2)'
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms')],
};
